import React, {Dispatch, ReactElement, SetStateAction} from 'react';
import {Image, ImageSourcePropType, Text, View} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import styles from './styles';

interface Props {
  label: string;
  image: ImageSourcePropType;
  placeHolder: string;
  placeHolderColor: string;
  isPassword?: boolean;
  setValue?: Dispatch<SetStateAction<string>>;
}

const Input = (props: Props): ReactElement => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      <View style={styles.groupIconInput}>
        <Image style={styles.icon} source={props.image} />
        <TextInput
          style={styles.input}
          placeholder={props.placeHolder}
          placeholderTextColor={props.placeHolderColor}
          secureTextEntry={props.isPassword ? true : false}
        />
      </View>
    </View>
  );
};
export default Input;
