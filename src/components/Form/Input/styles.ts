import {Palette} from '../../../theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  container: {
    height: 60,
    marginHorizontal: 30,
    marginTop: 20,
  },
  label: {
    color: Palette.color_545,
    fontSize: RFValue(10, 580),
  },
  groupIconInput: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: Palette.color_ccc,
  },
  icon: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginRight: 13,
  },
  input: {
    flex: 1,
  },
});
