import React, {ReactElement} from 'react';
import {
  GestureResponderEvent,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import styles from './styles';

interface Props {
  textQT: string;
  textLink: string;
  action?: ((event: GestureResponderEvent) => void) | undefined;
}

const TextLink = (props: Props): ReactElement => {
  return (
    <View style={styles.groupTextSignIn}>
      <Text style={styles.textSignIn}>{props.textQT}</Text>
      <TouchableOpacity onPress={props.action}>
        <Text style={[styles.textSignIn, styles.marginLeft5, styles.textLink]}>
          {props.textLink}
        </Text>
      </TouchableOpacity>
    </View>
  );
};
export default TextLink;
