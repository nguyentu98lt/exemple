import {Palette} from '../../theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  containerImage: {
    height: 70,
    width: 70,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: Palette.color_c6c,
    borderStyle: 'dashed',
    borderRadius: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconAdd: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 50,
  },
  modalContent: {
    width: 170,
    height: 90,
    backgroundColor: Palette.white,
    borderRadius: 10,
    borderWidth: 0.6,
    borderColor: Palette.color_ccc,
    shadowColor: Palette.black,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  modalCancel: {
    width: 170,
    height: 50,
    marginTop: 10,
    backgroundColor: Palette.white,
    borderRadius: 10,
    borderWidth: 0.6,
    borderColor: Palette.color_ccc,
    shadowColor: Palette.black,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  openButton: {
    borderTopWidth: 1,
    borderTopColor: Palette.color_ccc,
    padding: 10,
    zIndex: 1,
  },
  openButtonNoLine: {
    padding: 10,
    zIndex: 1,
  },
  textStyle: {
    fontSize: RFValue(15, 580),
    textAlign: 'center',
  },
  option: {
    color: Palette.color_3c8,
  },
  cancel: {
    color: Palette.color_ff2,
    fontWeight: '500',
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Palette.white,
    opacity: 0.6,
    zIndex: 0,
  },
});
