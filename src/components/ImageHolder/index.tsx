import media from '../../assets/media';
import React, {Dispatch, ReactElement, SetStateAction, useState} from 'react';
import {Image, Modal, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import styles from './styles';

interface Props {
  setValue?: Dispatch<SetStateAction<string>>;
}

const ImageHolder = (props: Props): ReactElement => {
  const [image, setImage] = useState('');
  const [chooseVisible, setChooseVisible] = useState(false);

  const open_gallery = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          setImage(img.path);
        })
        .catch(err => {
          console.log(err);
        });
    }, 100);
  };

  const open_camera = () => {
    setChooseVisible(false);
    setTimeout(() => {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      })
        .then(img => {
          setImage(img.path);
        })
        .catch(err => {
          console.log(err);
        });
    }, 100);
  };

  return (
    <>
      <TouchableOpacity onPress={() => (image ? null : setChooseVisible(true))}>
        {image ? (
          <View style={styles.containerImage}>
            <FastImage source={{uri: image}} style={styles.image} />
          </View>
        ) : (
          <View style={styles.containerImage}>
            <Image source={media.add} style={styles.iconAdd} />
          </View>
        )}
      </TouchableOpacity>

      <Modal transparent={true} visible={chooseVisible}>
        <View style={styles.overlay} />
        <View style={styles.centeredView}>
          <View style={styles.modalContent}>
            <TouchableOpacity
              style={styles.openButtonNoLine}
              onPress={() => open_camera()}>
              <Text style={[styles.textStyle, styles.option]}>{'Camera'}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.openButton}
              onPress={() => open_gallery()}>
              <Text style={[styles.textStyle, styles.option]}>{'Gallery'}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.modalCancel}>
            <TouchableOpacity
              style={styles.openButtonNoLine}
              onPress={() => {
                setChooseVisible(false);
              }}>
              <Text style={[styles.textStyle, styles.cancel]}>{'Cancel'}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};
export default ImageHolder;
