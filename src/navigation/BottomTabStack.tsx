import React, {} from 'react';
import {View, Text, StyleSheet, Platform, Image} from 'react-native';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import media from "../assets/media";
import {Palette} from "../theme/Palette";
import Account from "../containers/HomeSceens/Account";
import Notification from "../containers/HomeSceens/Notification";
import Pickup from "../containers/HomeSceens/Pickup";
import Statistics from "../containers/HomeSceens/Statistics";
import {createStackNavigator} from "@react-navigation/stack";
import ImageHeader from "../components/ImageHeader";
import {RFValue} from "react-native-responsive-fontsize";
import OrderStack from "./OrderStack";
import {ScreenMap} from "../config/NavigationConfig";
import {getBottomSpace} from 'react-native-iphone-x-helper'

const Stack= createStackNavigator();

function PickupStack  (){
    return(
        <Stack.Navigator>
            <Stack.Screen name='Pickup'
                          component={Pickup}
                          options={{
                              headerTitle: 'Lấy hàng',
                              headerLeft: () => {
                                  return<Image source={media.drawer} style={{width:16,height:16,marginLeft:10}}/>
                              },
                              headerBackground: () => <ImageHeader/>,
                              headerTintColor: Palette.white,
                              headerTitleStyle: {fontSize: RFValue(15, 580)},
                              headerTitleAlign: 'center',
                          }}/>
        </Stack.Navigator>
    )
}
function StatisticsStack  (){
    return(
        <Stack.Navigator>
            <Stack.Screen name='Statistics'
                          component={Statistics}
                          options={{
                              headerTitle: 'Thống kê',
                              headerLeft: () => {
                                  return<Image source={media.drawer} style={{width:16,height:16,marginLeft:10}}/>
                              },
                              headerBackground: () => <ImageHeader/>,
                              headerTintColor: Palette.white,
                              headerTitleStyle: {fontSize: RFValue(15, 580)},
                              headerTitleAlign: 'center',
                          }}/>
        </Stack.Navigator>
    )
}
function NotificationStack  (){
    return(
        <Stack.Navigator>
            <Stack.Screen name='Notification'
                          component={Notification}
                          options={{
                              headerTitle: 'Thông báo',
                              headerLeft: () => {
                                  return<Image source={media.drawer} style={{width:16,height:16,marginLeft:10}}/>
                              },
                              headerBackground: () => <ImageHeader/>,
                              headerTintColor: Palette.white,
                              headerTitleStyle: {fontSize: RFValue(15, 580)},
                              headerTitleAlign: 'center',
                          }}/>
        </Stack.Navigator>
    )
}
function AccountStack  (){
    return(
        <Stack.Navigator>
            <Stack.Screen name='Account'
                          component={Account}
                          options={{
                              headerTitle: 'Thông tin tài khoản',
                              headerLeft: () => {
                                  return<Image source={media.drawer} style={{width:16,height:16,marginLeft:10}}/>
                              },
                              headerBackground: () => <ImageHeader/>,
                              headerTintColor: Palette.white,
                              headerTitleStyle: {fontSize: RFValue(15, 580)},
                              headerTitleAlign: 'center',
                          }}/>
        </Stack.Navigator>
    )
}
const Tab = createBottomTabNavigator();
const BottomTabStack = () => {
    const getTabBarVisibility = (route: any) => {
        const routeName = route.state
            ? route.state.routes[route.state.index].name
            : '';

        if (
            routeName === ScreenMap.OrderDetail
        ) {
            return false;
        }

        return true;
    };
        return(
         <Tab.Navigator

             tabBarOptions={{
                 activeTintColor: Palette.color_e91,
                 labelStyle: {
                     fontSize: 14,
                 },
                 style: {height: 50, marginBottom:getBottomSpace(),paddingBottom:0},
             }}
             >
             <Tab.Screen name='Order'
                         component={OrderStack}
                         options={({route}) => ({
                             tabBarLabel: 'Đơn hàng',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.order_click : media.order}
                                     style={{width: 25, height: 25, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}
             />
             <Tab.Screen name='Pickup'
                         component={PickupStack}
                         options={({route}) => ({
                             tabBarLabel: 'Lấy hàng',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.pickup_click : media.pickup}
                                     style={{width:35, height: 35, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>
             <Tab.Screen name='Statistics'
                         component={StatisticsStack}
                         options={({route}) => ({
                             tabBarLabel: 'Thống kê',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.statistics_click : media.statistics}
                                     style={{width: 25, height: 25, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>
             <Tab.Screen name='Notification'
                         component={NotificationStack}
                         options={({route}) => ({
                             tabBarLabel: 'Thông báo',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.notification_click : media.notification}
                                     style={{width: 25, height: 25, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>
             <Tab.Screen name='Account'
                         component={AccountStack}
                         options={({route}) => ({
                             tabBarLabel: 'Tài khoản',
                             tabBarIcon: ({focused}) => (
                                 <Image
                                     source={focused ? media.account_click : media.account}
                                     style={{width: 25, height: 25, resizeMode: 'contain'}}
                                 />
                             ),
                             tabBarVisible: getTabBarVisibility(route),
                         })}/>


         </Tab.Navigator>
     )
}
export default BottomTabStack;
