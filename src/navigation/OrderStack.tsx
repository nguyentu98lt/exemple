import {createStackNavigator} from "@react-navigation/stack";
import ImageHeader from "../components/ImageHeader";
import {ScreenMap, ScreenParams} from "../config/NavigationConfig";
import Order from "../containers/HomeSceens/Order";
import DeatailOrderScreen from "../containers/HomeSceens/Order/Detail";
import {Palette} from "../theme/Palette";
import React from "react";
import {RFValue} from "react-native-responsive-fontsize";

const Stack = createStackNavigator<ScreenParams>();

const OrderStack = () => {
    return (
        <Stack.Navigator initialRouteName={ScreenMap.Order}>
            <Stack.Screen
                name={ScreenMap.Order}
                component={Order}
                options={{
                    headerTitle: 'Nhận đơn',
                    headerLeft: () => {
                        return null;
                    },
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
            <Stack.Screen
                name={ScreenMap.OrderDetail}
                component={DeatailOrderScreen}
                options={{
                    headerTitle: 'Chi tiết đơn hàng',
                    headerBackTitleVisible: false,
                    headerBackground: () => <ImageHeader />,
                    headerTintColor: Palette.white,
                    headerTitleStyle: {fontSize: RFValue(15, 580)},
                    headerTitleAlign: 'center',
                }}
            />
        </Stack.Navigator>
    );
};
export default OrderStack;
