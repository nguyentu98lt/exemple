import { get } from 'lodash';
import { createSelector } from 'reselect';
import { RootState } from '../redux';
import { HomeState, REDUX_KEY } from '../redux/HomeRedux';

export const selectHome = (state: RootState): HomeState => get(state, REDUX_KEY);

export const selectHomeData = createSelector(selectHome, (homeState: HomeState): {} => homeState.data);

export default {
  selectHomeData,
};
