import {ParamListBase} from '@react-navigation/native';

export enum ScreenMap {
  SignIn = 'SignIn',
  SignUp = 'SignUp',
  Otp = 'Otp',
  Provision = 'Provision',
  UpdateProfileInfo = 'UpdateProfileInfo',
  Order = 'Order',
  Statistics = 'Statistics',
  OrderDetail = 'OrderDetail',
  Notification = 'Notification',
}

export interface ScreenParams extends ParamListBase {
  [ScreenMap.SignIn]: {};
  [ScreenMap.SignUp]: {};
  [ScreenMap.Otp]: {};
  [ScreenMap.Provision]: {};
  [ScreenMap.UpdateProfileInfo]: {};
  [ScreenMap.Order]: {};
  [ScreenMap.OrderDetail]: { item: OrderItem };
  [ScreenMap.Statistics]: {};
  [ScreenMap.Notification]: {};
}
