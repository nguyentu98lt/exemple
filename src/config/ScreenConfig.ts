import {Color} from "../theme";
import {Dimensions} from 'react-native';

export const screenHeight = Math.round(Dimensions.get('window').height);
export const screenWidth = Math.round(Dimensions.get('window').width);

const ScreenConfig = {
  headerColor: Color.headerSafe,
  safeAreaColor: Color.headerSafe,
};
export default ScreenConfig;
