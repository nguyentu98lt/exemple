interface OrderItem {
  order_code: string;
  product_name: string;
  weight: string;
  amount: number;
  address: string;
  note: string;
}
