interface NotifyItem {
  id: string;
  title: string;
  send_date: string;
  content: string;
  create_date: string;
  isRead: boolean;
}
