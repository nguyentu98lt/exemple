import {StyleSheet} from 'react-native';
import {Palette} from "../../../theme/Palette";
import {RFValue} from "react-native-responsive-fontsize";

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',
    },
    item: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 10,
        paddingLeft: 16,
        paddingTop: 16,
        paddingBottom: 20,
        flexDirection: "row",
        width: '100%'
    },
    title: {
        fontSize: 16,
        fontWeight: "bold",
        marginBottom: 8
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 30,
        width: 110,
        height: 35,


    },
    itemHolder: {
        flex: 1,

        padding: 10,
        backgroundColor: Palette.white,



    },
    titleContent: {
        fontSize: RFValue(12, 580),
        fontWeight: 'bold',


    },
    address:{
        color:Palette.black,
        fontSize:14,
        fontWeight:"100",
        marginTop:8,
        marginBottom:4
    },
    btnHolder: {
        width: 150,
        position: "absolute",
        right:10,
    },

});
