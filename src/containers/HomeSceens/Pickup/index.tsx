import React, {useState} from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    Alert,
    Image,
    ScrollView
} from 'react-native';
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import {Palette} from "../../../theme/Palette";
import LinearGradient from "react-native-linear-gradient";
import styles from './style'
import media from "../../../assets/media";


const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Đơn 12345678',
        content: 'Thịt bò mỹ - 300g',
        amount: '1',
        address: 'Thịt bò Tây Ninh, Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        phonenumber1: '0988272733',
        phonenumber2: '0988272733',
        image: media.imageProduct2,
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Đơn 2534667',
        content: 'Thịt bò mỹ - 300g',
        amount: '1',
        address: 'Thịt bò Tây Ninh, Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        phonenumber1: '0988272733',
        phonenumber2: '0988272733',
        image: media.imageProduct1,

    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Đơn 12345678',
        content: 'Thịt bò mỹ - 300g',
        amount: '1',
        address: 'Thịt bò Tây Ninh, Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        phonenumber1: '0988272733',
        phonenumber2: '0988272733',
        image: media.imageProduct1,

    },
];


// @ts-ignore
const Item = ({title, content, amount, address, phonenumber1, phonenumber2}) => {

        const [text, setText] = useState('Nhận');
        const [chage, setChage] = useState(true);

        function onPress() {
            setText(
                "Đang lấy hàng"
            );
            if (text === "Đang lấy hàng") {
                Alert.alert('cdcdsc')
            }
        }


        return (
            <View style={styles.itemHolder}>
                <Text style={styles.titleContent}>{'Sản phẩm : ' + title}</Text>
                <Text style={[styles.address, {marginTop: 0}]}>{content}</Text>
                <Text style={[styles.address, {marginTop: 0}]}>{'SL: ' + amount}</Text>
                <Text
                    style={[styles.address, {marginTop: 0}]}>{address}</Text>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 1: '}</Text>
                    <Text>{phonenumber1}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 2: '}</Text>
                    <Text>{phonenumber2}</Text>
                </View>
                <Image source={media.imageProduct1} style={{width: 74, height: 68}}/>

                <View style={styles.btnHolder}>{chage ? (
                    <ButtonSubmit title='Lấy hàng' color={[Palette.color_ff0, Palette.color_ff4]} isSmall={true}
                                  action={() => {
                                      setChage(false)
                                  }}/>
                ) : (
                    <Text style={{
                        color: Palette.color_33c,
                        position: "absolute",
                        right: 16,
                        top: 10
                    }}>{'Đã lấy hàng'}</Text>
                )
                }
                </View>
                <View style={{borderBottomWidth: 0.2, marginTop: 10}}/>
            </View>
        )
    }
;

const Pickup = () => {
    // @ts-ignore
    const renderItem = ({item}) => (
        <Item title={item.title}
              content={item.content}
              amount={item.amount}
              address={item.address}
              phonenumber1={item.phonenumber1}
              phonenumber2={item.phonenumber2}/>

    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    );
}


export default Pickup;
