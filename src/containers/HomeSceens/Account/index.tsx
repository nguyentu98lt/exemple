import React, {} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import media from "../../../assets/media";
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import {Palette} from "../../../theme/Palette";

const Account = () => {
    return (
        <View style={styles.container}>
            <View style={{ width: '100%',
                backgroundColor: 'white',
                marginTop: 8,
                paddingTop: 14,
                paddingLeft: 16,
            paddingRight:16,
            paddingBottom:16}}>
                <View style={{
                    flexDirection: "row",

                }}>
                    <Image source={media.user} style={{width: 48, height: 48, marginRight: 16}}/>
                    <View style={{flex: 1}}>
                        <View>
                            <Text style={{fontWeight: "bold", fontSize: 16}}>{'Nguyễn Thành Công'}</Text>
                            <Text style={{fontSize: 14}}>{'0987654321'}</Text>
                        </View>
                        <TouchableOpacity style={{position: "absolute", right: 0}}>
                            <Image source={media.repair} style={{width: 17, height: 17}}/>
                        </TouchableOpacity>

                    </View>
                </View>
                <View style={{marginTop:18}}>
                    <View style={{flexDirection:"row",marginTop:10}}>
                        <Text style={{flex:1,fontSize:15,color:'#555555'}}>Ngày sinh</Text>
                        <Text style={{flex:1,textAlign:"right",fontSize:15,color:'#1f1f1f'}}>02/01/1990</Text>
                    </View>
                    <View style={{flexDirection:"row",marginTop:10}}>
                        <Text style={{flex:1,fontSize:15,color:'#555555'}}>Quê quán</Text>
                        <Text style={{flex:1,textAlign:"right",fontSize:15,color:'#1f1f1f'}}>Thanh Xuân, Hà Nội</Text>
                    </View>
                    <View style={{flexDirection:"row",marginTop:10}}>
                        <Text style={{flex:1,fontSize:15,color:'#555555'}}>Địa chỉ</Text>
                        <Text style={{flex:1,textAlign:"right",fontSize:15,color:'#1f1f1f'}} >18 Nguyễn Chí Thanh, Ngọc Khánh, Ba Đình, Hà Nội.</Text>
                    </View>
                </View>
            </View>
            <View style={{ width: '100%',
                backgroundColor: 'white',
                marginTop: 8,
                paddingTop: 14,
                paddingLeft: 16,
                paddingRight:16,
                paddingBottom:16}}>
                <View >
                    <Text style={{color:"#202020",fontSize:16,lineHeight:19}}>Thông tin tài khoản</Text>
                    <TouchableOpacity style={{position: "absolute", right: 0}}>
                        <Image source={media.repair} style={{width: 17, height: 17}}/>
                    </TouchableOpacity>
                </View>
                <View>
                    <View style={{marginTop:18}}>
                        <View style={{flexDirection:"row",marginTop:10}}>
                            <Text style={{flex:1,fontSize:15,color:'#555555'}}>Ngân hàng</Text>
                            <Text style={{flex:1,textAlign:"right",fontSize:15,color:'#1f1f1f'}}>NH TMCP Ngoại Thương VN</Text>
                        </View>
                        <View style={{flexDirection:"row",marginTop:10}}>
                            <Text style={{flex:1,fontSize:15,color:'#555555'}}>Số tài khoản</Text>
                            <Text style={{flex:1,textAlign:"right",fontSize:15,color:'#1f1f1f'}}>0491 000 147 236</Text>
                        </View>
                        <View style={{flexDirection:"row",marginTop:10}}>
                            <Text style={{flex:1,fontSize:15,color:'#555555'}}>Chủ tài khoản</Text>
                            <Text style={{flex:1,textAlign:"right",fontSize:15,color:'#1f1f1f'}} >Nguyễn Thành Công</Text>
                        </View>
                    </View>
                </View>
            </View>
            <ButtonSubmit title='Cập Nhât' color={[Palette.color_ff0,Palette.color_ff4]}/>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
});
export default Account;
