import {StyleSheet} from 'react-native';

export default  StyleSheet.create({
    container: {
        flex: 1,
        width:'100%'
    },
    item: {
        flex:1,
        backgroundColor: '#fff',
        marginTop: 10,
        paddingLeft: 16,
        paddingTop: 16,
        paddingBottom: 20,
        flexDirection:"row",
        width:'100%'
    },
    title: {
        fontSize: 16,
        fontWeight: "bold",
        marginBottom: 8
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding:10,
        borderRadius:30,
        width:110,
        height:35,



    },
    btnHolder: {
        width: 150,
        position: "absolute",
        right:10,
    },
});
