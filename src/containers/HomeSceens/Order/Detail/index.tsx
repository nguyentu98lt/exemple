import React, {ReactElement, useState} from 'react';
import {View, Text, StyleSheet, SafeAreaView, ScrollView, Image} from 'react-native';
import {BaseScreenProps} from "../../../../@types/screen-type";
import {ScreenMap} from "../../../../config/NavigationConfig";
import styles from "./style";
import {Palette} from "../../../../theme/Palette";
import media from "../../../../assets/media";
import ButtonSubmit from "../../../../components/Form/ButtonSubmit";
import ImageHolder from "../../../../components/ImageHolder";

const DeatailOrderScreen = (
    props: BaseScreenProps<ScreenMap.OrderDetail>,
): ReactElement => {
    const order_info = props.route.params.item;
    const [chage, setChage] = useState(true);
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
                <View style={styles.statusHolder}>
                    <Text style={{color: 'red', fontWeight: "bold", fontSize: 16, marginBottom: 8}}>
                        {'Đơn hàng: ' + order_info.order_code}
                    </Text>
                    <Text style={{color: Palette.color_828, fontSize: 14}}>
                        {'Số shop: ' + order_info.numbershop}
                    </Text>
                    <Text style={styles.address}>{'Địa chỉ:' + order_info.address}</Text>

                </View>
                <View style={styles.itemHolder}>
                    <View>
                        <Text style={styles.titleContent}>{'Sản phẩm 1: Đơn 12345678'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'Thịt bò mỹ - 300g'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'SL: 01'}</Text>
                        <Text
                            style={[styles.address, {marginTop: 0}]}>{'Thịt bò Tây Ninh, Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.'}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 1: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 2: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <Image source={media.imageProduct1} style={{width: 74, height: 68}}/>

                        <View style={styles.btnHolder}>{chage ? (
                            <ButtonSubmit title='Lấy hàng' color={[Palette.color_ff0, Palette.color_ff4]} isSmall={true}
                                          action={() => {
                                              setChage(false)
                                          }}/>
                        ) : (
                            <Text style={{
                                color: Palette.color_33c,
                                position: "absolute",
                                right: 16,
                                top: 10
                            }}>{'Đã lấy hàng'}</Text>
                        )
                        }
                        </View>
                        <View style={{borderBottomWidth: 0.2, marginTop: 10}}/>
                    </View>
                    <View>
                        <Text style={styles.titleContent}>{'Sản phẩm 2: Đơn 12345678'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'Thịt bò mỹ - 300g'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'SL: 01'}</Text>
                        <Text
                            style={[styles.address, {marginTop: 0}]}>{'Thịt bò Tây Ninh, Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.'}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 1: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 2: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <Image source={media.imageProduct1} style={{width: 74, height: 68}}/>

                        <View style={styles.btnHolder}>{chage ? (
                            <ButtonSubmit title='Lấy hàng' color={[Palette.color_ff0, Palette.color_ff4]} isSmall={true}
                                          action={() => {
                                              setChage(false)
                                          }}/>
                        ) : (
                            <Text style={{
                                color: Palette.color_33c,
                                position: "absolute",
                                right: 16,
                                top: 10
                            }}>{'Đã lấy hàng'}</Text>
                        )
                        }
                        </View>
                        <View style={{borderBottomWidth: 0.2, marginTop: 10}}/>
                    </View>
                    <View>
                        <Text style={styles.titleContent}>{'Sản phẩm 3: Đơn 12345678'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'Thịt bò mỹ - 300g'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'SL: 01'}</Text>
                        <Text
                            style={[styles.address, {marginTop: 0}]}>{'Thịt bò Tây Ninh, Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.'}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 1: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 2: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <Image source={media.imageProduct2} style={{width: 74, height: 68}}/>

                        <View style={styles.btnHolder}>{chage ? (
                            <ButtonSubmit title='Lấy hàng' color={[Palette.color_ff0, Palette.color_ff4]} isSmall={true}
                                          action={() => {
                                              setChage(false)
                                          }}/>
                        ) : (
                            <Text style={{
                                color: Palette.color_33c,
                                position: "absolute",
                                right: 16,
                                top: 10
                            }}>{'Đã lấy hàng'}</Text>
                        )
                        }
                        </View>
                        <View style={{borderBottomWidth: 0.2, marginTop: 10}}/>
                    </View>
                    <View>
                        <Text style={styles.titleContent}>{'Sản phẩm 3: Đơn 12345678'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'Thịt bò mỹ - 300g'}</Text>
                        <Text style={[styles.address, {marginTop: 0}]}>{'SL: 01'}</Text>
                        <Text
                            style={[styles.address, {marginTop: 0}]}>{'Thịt bò Tây Ninh, Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.'}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 1: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{color: Palette.color_4e4}}>{'Số điện thoại 2: '}</Text>
                            <Text>{'0988272733'}</Text>
                        </View>
                        <View style={{marginTop: 5}}>
                            <ImageHolder/>
                        </View>
                        <View style={styles.btnHolder}>{chage ? (
                            <ButtonSubmit title='Lấy hàng' color={[Palette.color_ff0, Palette.color_ff4]} isSmall={true}
                                          action={() => {
                                              setChage(false)
                                          }}/>
                        ) : (
                            <Text style={{
                                color: Palette.color_33c,
                                position: "absolute",
                                right: 16,
                                top: 10
                            }}>{'Đã lấy hàng'}</Text>
                        )
                        }
                        </View>
                        <View style={{borderBottomWidth: 0.2, marginTop: 10}}/>
                    </View>

                </View>
                <View style={{borderBottomWidth: 1}}/>
                <View style={{marginBottom: 40, margin: 10}}>
                    <Text style={{fontWeight: 'bold', fontSize: 16, marginBottom: 10}}>{'Địa chỉ giao hàng:'}</Text>
                    <Text>{order_info.delivery_address}</Text>
                    <View style={{marginTop:10}}>
                        <ImageHolder/>
                    </View>
                    <ButtonSubmit title='Giao hàng' color={[Palette.color_ff0, Palette.color_ff4]}/>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

export default DeatailOrderScreen;
