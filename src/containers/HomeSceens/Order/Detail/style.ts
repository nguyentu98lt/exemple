import {Palette} from "../../../../theme/Palette";
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Palette.color_fbf,
    },
    itemHolder: {
        flex: 1,
        marginTop: 10,
        padding: 10,
        backgroundColor: Palette.white,

        shadowColor: Palette.black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,

    },
    statusHolder: {
        flex: 1,
        marginTop: 10,
        padding: 10,
        backgroundColor: Palette.white,

        shadowColor: Palette.black,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        marginBottom: 15,
    },
    textContent: {
        fontSize: RFValue(10, 580),
        marginHorizontal: 8,
        marginTop: 7,
        color: Palette.color_3a3,
    },
    note: {
        color: Palette.color_ff4,
    },
    titleContent: {
        fontSize: RFValue(12, 580),
        fontWeight: 'bold',



    },
    statusOrder: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
    },
    alignTextCenterVertical: {
        alignSelf: 'center',
    },
    btnStatus: {
        width: 130,
        alignContent: 'center',
    },
    address:{
        color:Palette.black,
        fontSize:14,
        fontWeight:"100",
        marginTop:8,
        marginBottom:4
    },
    btnHolder: {
        width: 150,
        position: "absolute",
        right:10,
    },

});
export default styles;
