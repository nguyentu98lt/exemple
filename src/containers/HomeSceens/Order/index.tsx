import React, {ReactElement, useState} from 'react';
import {View, Text, StyleSheet, FlatList, SafeAreaView, StatusBar, TouchableOpacity, Alert} from 'react-native';
import ButtonSubmit from "../../../components/Form/ButtonSubmit";
import {Palette} from "../../../theme/Palette";
import LinearGradient from "react-native-linear-gradient";
import styles from './style'
import {StackNavigationProp} from "@react-navigation/stack";
import {ScreenMap, ScreenParams} from "../../../config/NavigationConfig";
import {BaseScreenProps} from "../../../@types/screen-type";


const DATA = [
    {

        order_code: '#DH0007',
        numbershop: '3',
        time: '10:30',
        date: '12/08/2020',
        address:' Tòa Nhà 28T Làng QT Thăng Long,  Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        delivery_address:'Tòa Nhà 28T Làng QT Thăng Long,  Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        note: 'Giao hàng trước 18h',
    },
    {

        order_code: '#DH0008',
        numbershop: '3',
        time: '10:30',
        date: '12/08/2020',
        address:' Tòa Nhà 28T Làng QT Thăng Long,  Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',

        delivery_address:'Tòa Nhà 28T Làng QT Thăng Long,  Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        note: 'Giao hàng trước 18h',
    },
    {

        order_code: '#DH0009',
        numbershop: '3',
        time: '10:30',
        date: '12/08/2020',
        address:' Tòa Nhà 28T Làng QT Thăng Long,  Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        delivery_address:'Tòa Nhà 28T Làng QT Thăng Long,  Trần Đăng Ninh,  Quận Cầu Giấy, Hà Nội.',
        note: 'Giao hàng trước 18h',

    },
];



// @ts-ignore
const Item = ({
                  navigation,
                  item,
              }: {
    navigation: StackNavigationProp<ScreenParams, ScreenMap.Order>;
    item: OrderItem
}) => {
    const [text, setText] = useState('Nhận');


    function onPress({}) {
        setText(
            "Đang lấy hàng"
        );
        if (text === 'Đang lấy hàng') {
            navigation.navigate('BottomTabStack', {
                screen: 'Order',
                params: {
                    screen: ScreenMap.OrderDetail,
                    params: {item: item},
                },
            })
        }

    }

    return (
        <View style={styles.item}>
            <View>
                <Text style={styles.title}>{'Đơn hàng: ' + item.order_code}</Text>
                <Text style={{
                    fontSize: 14,
                    lineHeight: 16,
                    color: '#828181',
                    marginBottom: 8
                }}>{'Số shop: ' + item.numbershop}</Text>
                <Text style={{
                    fontSize: 14,
                    lineHeight: 16,
                    color: '#828181',
                    marginBottom: 8
                }}>{item.time} {item.date}</Text>
                <Text style={{fontSize: 14, lineHeight: 16, color: '#FF4F04'}}>{'Lưu ý: ' + item.note}</Text>
            </View>
            <View style={styles.btnHolder}>
                <ButtonSubmit title={text} color={[Palette.color_ff0, Palette.color_ff4]} isSmall={true}
                              action={onPress}
                />
            </View>

        </View>
    )
};

const Order = (
    props: BaseScreenProps<ScreenMap.Order>,
): ReactElement => {
    const renderItem = ({item}: { item: OrderItem }) => {
        return <Item navigation={props.navigation} item={item}/>;
    };

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={item => item.order_code}
            />
        </SafeAreaView>
    );
}


export default Order;
