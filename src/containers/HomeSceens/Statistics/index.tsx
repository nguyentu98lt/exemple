import React, {ReactElement} from 'react';
import Today from "./Today";
import Week from "./Week";
import Month from "./Month";
import {Text} from "react-native";
import {SceneMap,TabBar,TabView} from "react-native-tab-view";
import {screenWidth} from "../../../config/ScreenConfig";
import {BaseScreenProps} from "../../../@types/screen-type";
import {ScreenMap} from "../../../config/NavigationConfig";
import {Palette} from "../../../theme/Palette";
const initialLayout = {width: screenWidth};

const TodayRoute = () =><Today/>
const WeekRoute = () =><Week/>
const MonthRoute = () =><Month/>
const Statistics = (props: BaseScreenProps<ScreenMap.Statistics>): ReactElement => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        {key: 'today', title: 'Hôm nay'},
        {key: 'week', title: 'Tuần'},
        {key: 'month', title: 'Tháng'},
    ]);
    const renderScene = SceneMap({
        today: TodayRoute,
        week: WeekRoute,
        month:MonthRoute,
    });
    const renderTabBar = (props: any) => (
        <TabBar
            {...props}
            getLabelText={({route}) => route.title}
            inactiveColor={Palette.black}
            activeColor={Palette.color_e91}
            indicatorStyle={{backgroundColor: Palette.white}}
            style={{backgroundColor: Palette.white}}
        />
    );
    return (
        <TabView
            renderTabBar={renderTabBar}
            navigationState={{index, routes}}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
        />
    );
}


export default Statistics;
