import React, {useState} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView} from 'react-native';
import {Palette} from "../../../../theme/Palette";
import media from "../../../../assets/media";
//import {AirbnbRating} from "react-native-ratings";
//import {AirbnbRating} from "../../../../Module/react-native-ratings";

const getCurrentDate = () => {

    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    if (month < 10) {
        return date + ' - ' + '0' + month + ' - ' + year;
    } else if (date < 10) {
        return '0' + date + '-' + month + '-' + year;//format: dd-mm-yyyy;
    } else {
        return date + '-' + month + '-' + year;
    }

    //Alert.alert(date + '-' + month + '-' + year);
    // You can turn it in to your desired format
    return date + '-' + month + '-' + year;//format: dd-mm-yyyy;
}

const Today = () => {

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
                <View style={{
                    backgroundColor: Palette.color_4a8,
                    flexDirection: "row",
                    height: 60,
                    justifyContent: "center",

                    borderRadius: 5
                }}>
                    <Text style={{
                        flex: 1,
                        alignSelf: "center",
                        color: Palette.white,
                        fontSize: 20,
                        paddingLeft: 15
                    }}>{'Tài khoản'}</Text>
                    <Text style={{alignSelf: "center", color: Palette.white, fontSize: 20, paddingRight: 15}}>25.000.000
                        đ</Text>
                </View>
                <View>
                    <View style={{flexDirection: "row", marginTop: 18}}>
                        <Text style={{flex: 1, fontSize: 17}}>{'Thống kê'}</Text>
                        <Text style={{fontSize: 14}}>{getCurrentDate()}</Text>
                        <Image source={media.calendar} style={{width: 18, height: 18, marginLeft: 6}}/>
                    </View>
                    <View style={{flexDirection: "row"}}>
                        <View style={{
                            backgroundColor: Palette.color_ff8,
                            flex: 1,
                            borderRadius: 5,
                            margin: 4,
                            marginRight: 8
                        }}>
                            <Text style={{color: Palette.white, padding: 16, fontSize: 14}}>{'Thu nhập'}</Text>
                            <Text style={{
                                color: Palette.white,
                                padding: 16,
                                paddingTop: 0,
                                fontSize: 17
                            }}>{'10,950,000 đ'}</Text>
                        </View>
                        <View style={{
                            backgroundColor: Palette.color_03B,
                            flex: 1,
                            borderRadius: 5,
                            margin: 4,
                            marginLeft: 8
                        }}>
                            <Text style={{color: Palette.white, padding: 16, fontSize: 14}}>{'Tổng đơn'}</Text>
                            <Text
                                style={{color: Palette.white, padding: 16, paddingTop: 0, fontSize: 17}}>{'125'}</Text>
                        </View>
                    </View>
                    <View style={{backgroundColor: Palette.color_6A6, borderRadius: 6}}>
                        <Text style={{
                            color: Palette.white,
                            padding: 16,
                            fontSize: 14,
                            paddingBottom: 8
                        }}>{'Trung bình thời giao hàng bị chậm'}</Text>
                        <Text style={{color: Palette.white, fontSize: 30, paddingLeft: 16}}>{'60 phút'}</Text>
                        <Text style={{
                            color: Palette.white,
                            padding: 16,
                            fontSize: 14,
                            paddingTop: 12
                        }}>{'Thời gian bị chậm hơn 120phút/ tháng sẽ tính phạt kpi'}</Text>
                    </View>
                </View>
                <View style={{backgroundColor: Palette.white, borderRadius: 6, marginTop: 10}}>
                    <View style={{flexDirection: "row", margin: 16}}>
                        <Text style={{flex: 1, fontSize: 16, color: Palette.color_333}}>{'Tổng đơn đặt:'}</Text>
                        <Text style={{fontSize: 16, fontWeight: "bold"}}>{'125'}</Text>
                    </View>
                    <View style={{flexDirection: "row", margin: 16, marginTop: 0}}>
                        <Text style={{flex: 1, fontSize: 16, color: Palette.color_333}}>{'Đơn thành công:'}</Text>
                        <Text style={{fontSize: 16, fontWeight: "bold"}}>{'115'}</Text>
                    </View>
                    <View style={{flexDirection: "row", margin: 16, marginTop: 0}}>
                        <Text style={{flex: 1, fontSize: 16, color: Palette.color_333}}>{'Đơn đã hủy:'}</Text>
                        <Text style={{fontSize: 16, fontWeight: "bold"}}>{'10'}</Text>
                    </View>

                </View>
                <View>
                    <Text style={{fontSize: 16, marginLeft: 20, marginTop: 10}}>{'Đánh giá '}</Text>
                    {/*<AirbnbRating/>*/}
                    <View style={{flexDirection: "row", alignItems: "center", justifyContent: "center", marginTop: 6}}>
                        <Text style={{fontSize: 14}}>{'(2 đánh giá/hôm nay) '}</Text>
                        <TouchableOpacity>
                            <Text style={{color: Palette.color_055, fontSize: 14}}> xem chi tiết...</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginRight: 15,
        marginLeft: 10,
        marginTop:20
    },
    childView: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 30,
    },
    StarImage: {
        width: 40,
        height: 40,
        resizeMode: 'cover',
    },
});
export default Today;
