import React, {} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Week = () => {
    return (
        <View style={styles.container}>
            <Text>Week</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
});
export default Week;
