import {BaseScreenProps} from '../../../@types/screen-type';
import ButtonSubmit from '../../../components/Form/ButtonSubmit';
import TextLink from '../../../components/Form/TextLink';
import {ScreenMap} from '../../../config/NavigationConfig';
import OTPInputView from '../../../Module/@twotalltotems/react-native-otp-input/dist';
import {Palette} from '../../../theme/Palette';
import React, {ReactElement} from 'react';
import {Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import styles from './styles';

const OtpScreen = (props: BaseScreenProps<ScreenMap.Otp>): ReactElement => {
  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(
  //     HomeActions.getHomeData(
  //       {},
  //       {
  //         onBeginning: () => {
  //           dispatch(GlobalAction.setShowLoading({isLoading: true}));
  //         },
  //         onSuccess: () => {},
  //         onFailure: () => {},
  //         onFinish: () => {
  //           setTimeout(() => {
  //             dispatch(GlobalAction.setShowLoading({isLoading: false}));
  //           }, 2000);
  //         },
  //       },
  //     ),
  //   );
  // }, [dispatch]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.otp}>
        <Text style={styles.otpTitle}>
          {'Nhập mã OTP vừa được gửi vào số điện thoại của bạn.'}
        </Text>
        <OTPInputView
          pinCount={4}
          autoFocusOnLoad
          style={styles.otpInput}
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          placeholderCharacter="-"
          placeholderTextColor={Palette.color_1e2}
        />
        <ButtonSubmit
          title="Xong"
          color={[Palette.color_ff0, Palette.color_ff4]}
          action={() => props.navigation.navigate(ScreenMap.Provision, {})}
        />
        <TextLink textQT="Bạn không nhận được mã OTP?" textLink="Gửi lại" />
      </View>
    </SafeAreaView>
  );
};
export default OtpScreen;
