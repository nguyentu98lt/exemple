import {Palette} from '../../../theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
  },
  otp: {flex: 1, justifyContent: 'center'},
  otpTitle: {
    color: Palette.color_6b6,
    fontSize: RFValue(10, 580),
    textAlign: 'center',
  },
  otpInput: {
    maxHeight: 100,
    width: '55%',
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 40,
    height: 50,
    borderWidth: 0,
    borderBottomWidth: 4,
    fontSize: RFValue(16, 580),
    fontWeight: '500',
    color: Palette.color_1e2,
  },
  underlineStyleHighLighted: {
    borderColor: Palette.color_ffc,
  },
});
