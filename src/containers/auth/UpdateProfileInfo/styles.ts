import {Palette} from '../../../theme/Palette';
import {Platform, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
    position:"absolute",
    bottom:20,
    width:'100%',
    height:'100%'
  },
  form: {
    flex: 1,
  },
  groupInput: {
    marginTop: 15,
    marginBottom: 30,
    zIndex: Platform.OS !== 'android' ? 10 : 0,
  },
  label: {
    color: Palette.color_545,
  },
  containerDate: {
    height: 60,
    marginHorizontal: 30,
    marginTop: 20,
  },
  groupIconDate: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: Palette.color_ccc,
  },
  icon: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginRight: 13,
  },
  groupRadio: {
    flex: 1,
    maxWidth: 200,
    flexDirection: 'row',
    marginTop: 15,
  },
  groupIconRadio: {
    flex: 1,
    flexDirection: 'row',
  },
  iconRadio: {
    width: 15,
    height: 15,
    marginRight: 10,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  dropdownContainer: {marginHorizontal: 27},
  conatinerPicker: {
    flex: 1,
    borderColor: '#8d8d8d',
    marginVertical: 15,
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginRight: 5,
    borderRadius: 4,
  },
  rowBet: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
    justifyContent: 'space-between',
  },

});
export default styles;
