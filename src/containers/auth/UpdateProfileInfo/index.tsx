import {BaseScreenProps} from '../../../@types/screen-type';
import media from '../../../assets/media';
import ButtonSubmit from '../../../components/Form/ButtonSubmit';
import Input from '../../../components/Form/Input';
import {ScreenMap} from '../../../config/NavigationConfig';
import {Palette} from '../../../theme/Palette';
import moment from 'moment';
import React, {Dispatch, Props, ReactElement, useState} from 'react';
import {Image, SafeAreaView, Text, TouchableOpacity, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDispatch} from 'react-redux';
import DatePicker from '../../../Module/react-native-datepicker/datepicker';
import styles from './styles';

import {RFValue} from "react-native-responsive-fontsize";
import ImageHolder from "../../../components/ImageHolder";
import {Dropdown} from "../../../Module/react-native-material-dropdown";



const UpdateProfileInfo = (
    props: BaseScreenProps<ScreenMap.UpdateProfileInfo>,
): ReactElement => {
    const gender_type = {man: 1, woman: 0};

    const [date, setDate] = useState('');
    // useEffect(() => {
    //   dispatch(
    //     HomeActions.getHomeData(
    //       {},
    //       {
    //         onBeginning: () => {
    //           dispatch(GlobalAction.setShowLoading({isLoading: true}));
    //         },
    //         onSuccess: () => {},
    //         onFailure: () => {},
    //         onFinish: () => {
    //           setTimeout(() => {
    //             dispatch(GlobalAction.setShowLoading({isLoading: false}));
    //           }, 2000);
    //         },
    //       },
    //     ),
    //   );
    // }, [dispatch]);
    const [gender, setGender] = useState(1);
    const renderDatePicker = () => {
        return (
            <View style={styles.containerDate}>
                <Text style={styles.label}>{'Ngày sinh'}</Text>
                <View style={styles.groupIconDate}>
                    <Image style={styles.icon} source={media.calendar}/>
                    <DatePicker
                        date={date}
                        mode="date"
                        placeholder="dd-mm-yyyy"
                        format="DD-MM-YYYY"
                        minDate="01-05-2000"
                        maxDate={moment().format('DD-MM-YYYY')}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                opacity: 0,
                            },
                            dateInput: {
                                flex: 1,
                                alignItems: 'flex-start',
                                borderWidth: 0,
                            },
                        }}
                        onDateChange={(date: string) => {
                            setDate(date);
                        }}
                    />

                </View>

            </View>
        );
    };

    const renderRadioGroup = () => {
        return (
            <View style={styles.containerDate}>
                <Text style={styles.label}>{'Giới tính'}</Text>
                <View style={styles.groupRadio}>
                    <View style={styles.groupIconRadio}>
                        <TouchableOpacity onPress={() => setGender(1)}>
                            <Image
                                source={
                                    gender == gender_type.man
                                        ? media.radio_checked
                                        : media.radio_unchecked
                                }
                                style={styles.iconRadio}
                            />
                        </TouchableOpacity>
                        <Text>{'Nam'}</Text>
                    </View>
                    <View style={styles.groupIconRadio}>
                        <TouchableOpacity onPress={() => setGender(0)}>
                            <Image
                                source={
                                    gender == gender_type.woman
                                        ? media.radio_checked
                                        : media.radio_unchecked
                                }
                                style={styles.iconRadio}
                            />
                        </TouchableOpacity>
                        <Text>{'Nữ'}</Text>
                    </View>
                </View>
            </View>
        );
    };

    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView>
                <View style={styles.form}>
                    <View style={styles.groupInput}>
                        <Input
                            label="Họ và tên"
                            image={media.user}
                            placeHolder="Nhập họ và tên của bạn"
                            placeHolderColor={Palette.color_ccc}
                        />

                        {renderDatePicker()}
                        {renderRadioGroup()}
                        <Dropdown
                            label="Quận/Huyện"
                            data={[
                                {label: 'Ba Đình', value: '1'},
                                {label: 'Tây Hồ', value: '2'},
                            ]}
                            containerStyle={styles.dropdownContainer}
                            fontSize={RFValue(10, 580)}
                        />
                        <Dropdown
                            label="Phường/Xã"
                            data={[
                                {label: 'Bưởi', value: '1'},
                                {label: 'Thụy Khuê', value: '2'},
                            ]}
                            containerStyle={styles.dropdownContainer}
                            fontSize={RFValue(10, 580)}
                        />
                        <Input
                            label="Địa chỉ"
                            image={media.location}
                            placeHolder="Địa chỉ cụ thể"
                            placeHolderColor={Palette.color_ccc}
                        />

                        <Dropdown
                            label="Tòa nhà nhận ship"
                            data={[
                                {label: 'Tòa nhà Vietcom', value: '1'},
                                {label: 'Tòa nhà HH2 Bắc Hà', value: '2'},
                            ]}
                            containerStyle={styles.dropdownContainer}
                            fontSize={RFValue(10, 580)}
                        />
                    </View>
                    <View style={{marginLeft:22}}>
                        <Text style={{padding:6,paddingTop:0}}>{'Cập nhật ảnh chụp mặt và chứng minh thư 2 mặt'}</Text>
                        <View style={{flexDirection:"row",marginTop:10,marginBottom:10}}>
                            <ImageHolder/>
                            <ImageHolder/>
                            <ImageHolder/>
                        </View>
                    </View>
                    <ButtonSubmit
                        title="Cập nhật"
                        color={[Palette.color_ff0, Palette.color_ff4]}
                        action={() =>
                            props.navigation.navigate(ScreenMap.SignIn, {})
                        }
                    />
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    );
};
export default UpdateProfileInfo;
