import {Palette} from '../../../theme/Palette';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
  },
  groupContent: {
    flex: 1,
    marginBottom: 40,

  },
  groupSubmit: {
    marginTop: 10,
  },
  groupTextBelow: {
    height: 30,
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
  },
  hairline: {
    flex: 1,
    height: 0.5,
    backgroundColor: Palette.color_ccc,
  },
  loginButtonBelowText1: {
    width: 130,
    textAlign: 'center',
    color: Palette.color_ccc,
  },
});
