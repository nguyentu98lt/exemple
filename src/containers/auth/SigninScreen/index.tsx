import {BaseScreenProps} from '../../../@types/screen-type';
import {ScreenMap} from '../../../config/NavigationConfig';
import {Palette} from '../../../theme/Palette';
import React, {ReactElement} from 'react';
import {Text, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import media from '../../../assets/media';
import ButtonSocial from '../../../components/Form/ButtonSocial';
import ButtonSubmit from '../../../components/Form/ButtonSubmit';
import Input from '../../../components/Form/Input';
import styles from './styles';

const SignInScreen = (
  props: BaseScreenProps<ScreenMap.SignIn>,
): ReactElement => {
  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(
  //     HomeActions.getHomeData(
  //       {},
  //       {
  //         onBeginning: () => {
  //           dispatch(GlobalAction.setShowLoading({isLoading: true}));
  //         },
  //         onSuccess: () => {},
  //         onFailure: () => {},
  //         onFinish: () => {
  //           setTimeout(() => {
  //             dispatch(GlobalAction.setShowLoading({isLoading: false}));
  //           }, 2000);
  //         },
  //       },
  //     ),
  //   );
  // }, [dispatch]);

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAwareScrollView>
        <View style={styles.groupContent}>
          <Input
            label="Số điện thoại"
            image={media.phone}
            placeHolder="Nhập số điện thoại của bạn"
            placeHolderColor={Palette.color_ccc}
          />
          <Input
            label="Mật khẩu"
            image={media.lock}
            placeHolder="*******"
            placeHolderColor={Palette.color_ccc}
            isPassword={true}
          />
          <View style={styles.groupSubmit}>
            <ButtonSubmit
              title="Đăng Nhập"
              color={[Palette.color_ff0, Palette.color_ff4]}
              action={() => {
                props.navigation.navigate('BottomTabStack', {
                  screen: 'Oder',
                  params: {
                    screen: ScreenMap.Order,
                  },
                });
              }}
            />
            <ButtonSubmit
              title="Đăng Ký"
              color={[Palette.color_f48, Palette.color_f48]}
              action={() => props.navigation.navigate(ScreenMap.SignUp, {})}
            />
          </View>
          <View style={styles.groupTextBelow}>
            <View style={styles.hairline} />
            <Text style={styles.loginButtonBelowText1}>{'Đăng nhập bằng'}</Text>
            <View style={styles.hairline} />
          </View>
          <ButtonSocial
            title="Đăng Nhập bằng tài khoản Facebook"
            color={[Palette.color_055, Palette.color_055]}
            image={media.facebook}
          />
          <ButtonSocial
            title="Đăng Nhập bằng tài khoản Zalo"
            color={[Palette.color_0f9, Palette.color_0f9]}
            image={media.zalo}
          />
          <ButtonSocial
            title="Đăng Nhập bằng tài khoản Google"
            color={[Palette.color_f01, Palette.color_f01]}
            image={media.google}
          />
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
export default SignInScreen;
