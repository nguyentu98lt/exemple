import {BaseScreenProps} from '../../../@types/screen-type';
import {ScreenMap} from '../../../config/NavigationConfig';
import {Palette} from '../../../theme/Palette';
import React, {ReactElement} from 'react';
import {View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import media from '../../../assets/media';
import ButtonSubmit from '../../../components/Form/ButtonSubmit';
import Input from '../../../components/Form/Input';
import TextLink from '../../../components/Form/TextLink';
import styles from './styles';

const SignUpScreen = (
  props: BaseScreenProps<ScreenMap.SignUp>,
): ReactElement => {
  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(
  //     HomeActions.getHomeData(
  //       {},
  //       {
  //         onBeginning: () => {
  //           dispatch(GlobalAction.setShowLoading({isLoading: true}));
  //         },
  //         onSuccess: () => {},
  //         onFailure: () => {},
  //         onFinish: () => {
  //           setTimeout(() => {
  //             dispatch(GlobalAction.setShowLoading({isLoading: false}));
  //           }, 2000);
  //         },
  //       },
  //     ),
  //   );
  // }, [dispatch]);

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAwareScrollView>
        <View style={styles.form}>
          <Input
            label="Số điện thoại"
            image={media.phone}
            placeHolder="Nhập số điện thoại của bạn"
            placeHolderColor={Palette.color_ccc}
          />
          <Input
            label="Mật khẩu"
            image={media.lock}
            placeHolder="*******"
            placeHolderColor={Palette.color_ccc}
            isPassword={true}
          />
          <Input
            label="Nhập lại mật khẩu"
            image={media.lock}
            placeHolder="*******"
            placeHolderColor={Palette.color_ccc}
            isPassword={true}
          />
          <ButtonSubmit
            title="Đăng Ký"
            color={[Palette.color_ff0, Palette.color_ff4]}
            action={() => props.navigation.navigate(ScreenMap.Otp, {})}
          />
          <TextLink
            textQT="Bạn đã có tài khoản?"
            textLink="Đăng Nhập"
            action={() => props.navigation.navigate(ScreenMap.SignIn, {})}
          />
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};
export default SignUpScreen;
