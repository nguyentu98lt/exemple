import {Palette} from '../../../theme/Palette';
import {StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Palette.white,
  },
  form: {
    flex: 1,
  },
  groupTextSignIn: {
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 15,
  },
  textSignIn: {
    fontSize: RFValue(12, 580),
    color: Palette.color_1f1,
  },
  marginLeft5: {
    marginLeft: 5,
  },
  textLink: {
    color: Palette.color_e40,
  },
});
