/**
 * @format
 */

import {AppRegistry} from 'react-native';

import {name as appName} from './app.json';
import App from './src/containers/App';
import 'react-native-gesture-handler';
console.disableYellowBox=true

AppRegistry.registerComponent(appName, () => App);
